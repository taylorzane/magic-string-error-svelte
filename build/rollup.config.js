import svelte from 'rollup-plugin-svelte'

export default {
  entry: 'src/index.js',
  dest: 'dist/index.js',
  format: 'umd',
  moduleName: 'test-failure',
  plugins: [
    svelte({
      dev: process.env.ENV === 'production' ? false : true
    })
  ]
}
